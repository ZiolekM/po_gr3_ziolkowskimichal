package lab12.lab12;

import java.util.*;
import static lab12.lab12.Lab12.odwroc;
import static lab12.lab12.Lab12.odwrocLiczbe;
import static lab12.lab12.Lab12.redukuj;
import static lab12.lab12.Lab12.Erastotenes;


public class Main 
{
    public static void main(String[] args)
    {

        LinkedList<Integer> lista = new LinkedList<>();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        lista.add(5);
        lista.add(6);
        lista.add(7);
        System.out.println(lista);
        odwroc(lista);
        System.out.println(lista);


        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Michal");
        pracownicy.add("Konrad");
        pracownicy.add("Wojtek");
        pracownicy.add("Mikolaj");
        System.out.println(pracownicy);
        redukuj(pracownicy, 2);
        System.out.println(pracownicy);

        
        int liczba = 1234;
        odwrocLiczbe(liczba);
        System.out.println();
   
      
        Erastotenes(12);
    }
}
