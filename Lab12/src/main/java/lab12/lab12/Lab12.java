package lab12.lab12;

import java.util.*;


public class Lab12
{
    public static <T> void redukuj(LinkedList<T>pracownicy, int n)
    {
        for(int i = n - 1; i < pracownicy.size(); i += n - 1)
        {
            pracownicy.remove(i);
        }
    }

    
    public static<T> void odwroc(LinkedList<T> lista)
    {
        int j=lista.size()-1;
        for(int i = 0; i < lista.size()/2; i++)
        {
            T tmp = lista.get(j);
            lista.set(j, lista.get(i));
            lista.set(i, tmp);
            j--;
        }
    }
    
    
    public static void odwrocLiczbe(int n)
    {
        Stack<Integer> stos = new Stack<>();
        while(n != 0)
        {
            stos.add(n % 10);
            n = n / 10;
        }
        Iterator<Integer> tmp = stos.iterator();
        while(tmp.hasNext())
        {
            System.out.print(stos.pop() + " ");
        }
    }

    
    public static void Erastotenes(int n)
    {
        boolean[] pierwsze = new boolean[n];
        Arrays.fill(pierwsze, true);
        pierwsze[0] = false;
        pierwsze[1] = false;
        for(int i = 2; i < Math.sqrt(pierwsze.length); i++)
        {
            for(int j = i * i; j < pierwsze.length; j += i)
            {
                if(pierwsze[j])
                {
                    pierwsze[j] = false;
                }
            }
        }
        for(int i = 0; i < pierwsze.length; i++)
        {
            if(pierwsze[i])
            {
                System.out.print(i + " ");
            }
        }
    }

    
    public static<T extends Iterable<T>> void print(T zmienna)
    {
        Iterator<T> tmp = zmienna.iterator();
        while(tmp.hasNext())
        {
            System.out.print(tmp.next() + ", ");
        }
    }
}