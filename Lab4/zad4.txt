package projekt2;
import java.util.Scanner;
import java.util.Arrays;
import java.math.BigInteger;

public class Projekt2 {
    
    public static void policz(int n)
    {
        
        BigInteger wynik = new BigInteger("1");
        BigInteger dwa = new BigInteger("2");
        BigInteger result = new BigInteger("0");
        
        int a = n*n;
        result = dwa.pow(a).subtract(wynik);
        System.out.println(result);
    }



public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Podaj n: ");
    int napis = scan.nextInt();
    policz(napis);
    }
}