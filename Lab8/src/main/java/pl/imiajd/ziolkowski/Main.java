package pl.imiajd.ziolkowski;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

//testy dla zad_1
    public static void main(String[] args){

    String[] testImiona = {"Michal", "Jan", "Krzysztof"};
    Osoba pierwsza = new Osoba(testImiona, "Ziolkowski", 2000, 1, 27, false);
        System.out.println(Arrays.toString(pierwsza.getImiona()));
        System.out.println(pierwsza.getDataUrodzenia());
        System.out.println(pierwsza.getPlec());


    Pracownik pierwszy = new Pracownik(testImiona, "Kowalski", 1989, 7, 9, false, 3000.0, 2001, 12, 1);
        System.out.println(pierwszy.getDataZatrudnienia());


    Student pierwszy2 = new Student(testImiona, "Niedzielski", 2000, 10, 11, false, "Prawo", 4.0);
        System.out.print(pierwszy2.getSredniaOcen());
        
        
    System.out.println(); 
    
    //testy dla zad 3
    ArrayList<Instrument> Orkiestra = new ArrayList<Instrument>();

    Orkiestra.add(new Flet("Flet tenorowy", 2020));
    Orkiestra.add(new Flet("Flet basowy", 2019));
    Orkiestra.add(new Flet("Flet altowy", 2018));
    Orkiestra.add(new Fortepian("Fortepian gabinetowy", 2017));
    Orkiestra.add(new Skrzypce("Skrzypce Stradivariego", 2016));

        for(int i = 0; i<Orkiestra.size(); i++)
            Orkiestra.get(i).dzwiek();

        for(int i = 0; i<Orkiestra.size(); i++)
            System.out.println(Orkiestra.get(i).toString());
    }
}