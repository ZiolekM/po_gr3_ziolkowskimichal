import pl.imiajd.ziolkowski1.Osoba;
import java.util.ArrayList;
import java.util.Collections;


public class testOsoba 
    {
    public static void main(String[] args){
        ArrayList<Osoba> grupa = new ArrayList<Osoba>();
        grupa.add(new Student("Nowosilcov", 1975, 10, 10, 3.5));
        grupa.add(new Student("Novosilcov", 1975, 10, 10, 3.5));
        grupa.add(new Student("Kowalski", 1998, 12, 31, 5.0));
        grupa.add(new Student("Skipper", 1998, 12, 31, 5.0));
        grupa.add(new Student("Ziolkowski", 2000, 1, 27, 5.0));

        Collections.sort(grupa);

        for(int i = 0; i<5; i++)
            System.out.println(grupa.get(i).toString());
    }
}