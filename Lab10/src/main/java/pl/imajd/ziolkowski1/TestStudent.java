import pl.imiajd.ziolkowski.Student;
import java.util.ArrayList;
import java.util.Collections;


public class testStudent 
    {
    public static void main(String[] args){
        ArrayList<Student> grupa = new ArrayList<Student>();
        grupa.add(new Student("Nowosilcov", 1975, 10, 10, 3.5));
        grupa.add(new Student("Novosilcov", 1975, 10, 10, 3.5));
        grupa.add(new Student("Kowalski", 1998, 12, 31, 5.0));
        grupa.add(new Student("Skipper", 1998, 12, 31, 5.0));
        grupa.add(new Student("Ziolkowski", 2000, 1, 27, 5.0));

        Collections.sort(grupa);

        for(int i = 0; i<5; i++)
            System.out.println(grupa.get(i).toString());
    }
}