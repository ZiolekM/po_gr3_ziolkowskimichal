package laboratorium11.laboratorium11;

import java.util.ArrayList;

public class ArrayUtil 
{
    public static <T extends Comparable<? super T>> boolean isSorted(T tab[])
    {
        boolean helper = true;

        for(int i = 1; i<tab.length; i++)
        {
            if(tab[i].compareTo(tab[i-1]) < 0)
            {
                helper = false;
            }
        }
        return helper;
    }

    public static <T extends Comparable<? super T>> int binSearch(T tab[], T element)
    {
        int x = 0;
        int y = tab.length-1;
        int idx;
        int result;

        while(x<=y)
        {
            idx = (x+y)/2;
            result = tab[idx].compareTo(element);
            if(result > 0)
            {
                y = idx-1;
            }
            if(result < 0)
            {
                x = idx+1;
            }
            if(result == 0)
            {
                return idx;
            }
        }
        return -1;
    }

    public static <T extends Comparable<? super T>> void selectionSort(T tab[]) 
    {
        int idx = 0;
        T min, helper;
        int min_idx;

        for (int i = 0; i < tab.length; i++) 
        {
            min = tab[idx];
            min_idx = idx;
            for (int j = idx + 1; j < tab.length; j++) 
            {
                if (tab[j].compareTo(min) < 0) 
                {
                    min = tab[j];
                    min_idx = j;
                }
            }
            helper = tab[idx];
            tab[idx] = min;
            tab[min_idx] = helper;
            idx++;
        }
    }
}
