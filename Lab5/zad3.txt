package javaapplication4;
import java.util.ArrayList;
import java.util.Arrays;


public class JavaApplication4 {


    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> newArray = new ArrayList<Integer>();
        int min1 = 0;
        int pos1 = 0;
        int min2 = 0;
        int pos2 = 0;
        int iterator=0;
        boolean[] a_empty = new boolean[a.size()];
        boolean[] b_empty = new boolean[b.size()];

        for(int i = 0; i<a.size(); i++)
        {
            a_empty[i] = false;
        }

        for(int i = 0; i<b.size(); i++)
        {
            b_empty[i] = false;
        }
        int a_ite = a.size();
        int b_ite = b.size();

        while(a_ite > 0 && b_ite > 0){
            iterator = 0;

            while(a_empty[iterator])
            {
                iterator++;
            }
            min1 = a.get(iterator);
            pos1 = iterator;

            for(int i = 0; i<a.size(); i++)
            {
                if(a.get(i) < min1 && !a_empty[i])
                {
                    min1 = a.get(i);
                    pos1 = i;
                }
            }

            iterator = 0;
            while(b_empty[iterator])
            {
                iterator++;
            }

            min2 = b.get(iterator);
            pos2 = iterator;

            for(int i = 0; i<b.size(); i++)
            {
                if(b.get(i) < min2 && !b_empty[i])
                {
                    min2 = b.get(i);
                    pos2 = i;
                }
            }
            if(min2 > min1)
                {
                a_empty[pos1] = true;
                a_ite--;
                newArray.add(min1);
            }
            else
            {
                b_empty[pos2] = true;
                b_ite--;
                newArray.add(min2);
            }
        }
        while(a_ite > 0)
            {
            iterator = 0;
            while(a_empty[iterator])
            {
                iterator++;
            }
            min1 = a.get(iterator);
            pos1 = iterator;

            for(int i = 0; i<a.size(); i++)
            {
                if(a.get(i) < min1 && !a_empty[i])
                {
                    min1 = a.get(i);
                    pos1 = i;
                }
            }
            a_empty[pos1] = true;
            a_ite--;
            newArray.add(min1);
        }
        while(b_ite > 0)
            {
            iterator = 0;
            while(b_empty[iterator])
            {
                iterator++;
            }
            min2 = b.get(iterator);
            pos2 = iterator;
            for(int i = 0; i<b.size(); i++)
            {
                if(b.get(i) < min2 && !b_empty[i])
                {
                    min2 = b.get(i);
                    pos2 = i;
                }
            }

            b_empty[pos2] = true;
            b_ite--;
            newArray.add(min2);
        }

        return newArray;
    }


    public static void main(String[] args) {

        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        ArrayList<Integer> b = new ArrayList<Integer>(Arrays.asList(9,7,4,9,11));
        ArrayList<Integer> test3 = mergeSorted(a, b);
        System.out.println(test3);
    }
}